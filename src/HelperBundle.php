<?php declare(strict_types=1);

namespace Persist\HelperBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class HelperBundle extends Bundle
{
}
