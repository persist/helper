<?php declare(strict_types=1);

namespace Persist\HelperBundle\Util\Json;

class Json
{
    /**
     * @param string $input
     *
     * @return array
     */
    public static function decode(string $input): array
    {
        if (is_string($input) && empty($input)) {
            return [];
        }

        $response = json_decode($input, true);

        return is_array($response) ? $response : [$response];
    }

    /**
     * @param array $input
     * @param int $options
     *
     * @return string
     */
    public static function encode(array $input, int $options = JSON_UNESCAPED_UNICODE): string
    {
        return json_encode($input, $options);
    }
}
