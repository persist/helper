<?php declare(strict_types=1);

namespace Persist\HelperBundle\Util\Date;

trait DateUtilAwareTrait
{
    /** @var DateUtil */
    private $dateUtil;

    /**
     * @return DateUtil
     */
    public function getDateUtil(): DateUtil
    {
        if ($this->dateUtil === null) {
            $this->dateUtil = new DateUtil();
        }

        return $this->dateUtil;
    }

    /**
     * @param DateUtil $dateUtil
     */
    public function setDateUtil(DateUtil $dateUtil)
    {
        $this->dateUtil = $dateUtil;
    }
}
