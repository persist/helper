<?php declare(strict_types=1);

namespace Persist\HelperBundle\Util\Date;

class DateUtil
{
    /** @var \DateTime */
    private $dateTime;

    /**
     * @param \DateTime $dateTime
     */
    public function __construct(\DateTime $dateTime = null)
    {
        $this->dateTime = $dateTime;
    }

    /**
     * @param string $date
     *
     * @return DateUtil
     */
    public static function fromString(string $date): self
    {
        return new self(new \DateTime($date));
    }

    /**
     * @param \DateTime $dateTime
     */
    public function setDateTime(\DateTime $dateTime)
    {
        $this->dateTime = $dateTime;
    }

    /**
     * @param string $time
     */
    public function setTime(string $time)
    {
        $this->setDateTime(new \DateTime($time));
    }

    /**
     * @return \DateTime
     */
    public function getDateTime(): \DateTime
    {
        if ($this->dateTime === null) {
            $this->dateTime = new \DateTime();
        }

        return $this->dateTime;
    }
}
