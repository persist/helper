<?php declare(strict_types=1);

namespace Persist\HelperBundle\Util\Generator;

use Ramsey\Uuid\Uuid as Ramsey;

class Uuid
{
    /**
     * @return string
     */
    public static function generate(): string
    {
        return Ramsey::uuid1()->toString();
    }
}
