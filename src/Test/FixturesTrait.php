<?php declare(strict_types=1);

namespace Persist\HelperBundle\Test;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Config\FileLocator;

trait FixturesTrait
{
    /**
     * @param string $dir
     * @param string $fixture
     * @param string $name
     *
     * @return array
     */
    protected function loadYamlFixture(string $dir, string $fixture, string $name = null)
    {
        $fixtureFile = $this->locateFixture($dir, sprintf('%s.yaml', $fixture));

        return $this->readYmlFixture($fixtureFile, $name);
    }

    /**
     * @param string $dir
     * @param string $fixture
     *
     * @return string
     */
    protected function loadHtmlFixture(string $dir, string $fixture): string
    {
        return $this->loadFixture($dir, $fixture, 'html');
    }

    /**
     * @param string $dir
     * @param string $fixture
     *
     * @return string
     */
    protected function loadXmlFixture(string $dir, string $fixture): string
    {
        return $this->loadFixture($dir, $fixture, 'xml');
    }

    /**
     * @param string $dir
     * @param string $fixture
     * @param string $extension
     *
     * @return string
     */
    protected function loadFixture(string $dir, string $fixture, string $extension): string
    {
        $fixtureFile = $this->locateFixture($dir, sprintf('%s.%s', $fixture, $extension));

        return file_get_contents($fixtureFile);
    }

    /**
     * @param string $dir
     * @param string $fixture
     *
     * @return string
     */
    protected function locateFixture(string $dir, string $fixture): string
    {
        $locator = new FileLocator($dir . '/fixtures');

        return $locator->locate($fixture);
    }

    /**
     * @param string $fixture
     * @param string $name
     *
     * @return mixed
     */
    protected function readYmlFixture(string $fixture, string $name = null)
    {
        $contents = Yaml::parse(file_get_contents($fixture));

        return $name !== null ? ($contents[$name] ?? null) : $contents;
    }
}
